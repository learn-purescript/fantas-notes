# What is this about?

Notes and exercises for the articles of Tom Harding regarding functional programming and the tiny library daggy.

The posts of the series `Fatans, Eel and Specification` can be found here:

- [1: Daggy](http://www.tomharding.me/2017/03/03/fantas-eel-and-specification/)
- [2: Type Signatures](http://www.tomharding.me/2017/03/08/fantas-eel-and-specification-2/)
- [3: Setoid](http://www.tomharding.me/2017/03/09/fantas-eel-and-specification-3/)
- [4: Ord](http://www.tomharding.me/2017/04/09/fantas-eel-and-specification-3.5/)
- [5: SemiGroup](http://www.tomharding.me/2017/03/13/fantas-eel-and-specification-4/)
- [6: Monoid](http://www.tomharding.me/2017/03/21/fantas-eel-and-specification-5/)
- [7: Functor](http://www.tomharding.me/2017/03/27/fantas-eel-and-specification-6/)