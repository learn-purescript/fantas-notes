## 01 - Daggy

## 02 - Type Signatures

```
equals :: Setoid a => a -> a -> Bool
```

`=>` means that the signature to its _right_ is valid if all the conditions th its _left_ are satisfied.

In this case of `equals`, the signature `a -> a -> Bool` is valid if `a` is a `Setoid`.

```
equals :: Setoid a => a ~> a -> Boolean
```

`~>` means that `equals` is a _method_ on the thing to the left of `~>`, and the thing to the right is its signature.

For example:

```
toArray :: List a ~> [a]
```

We are saying that a `List` of value with type `a` has a method called `toArray` that returns an array of type `a`.

## 03 - Setoid

### Definition Setoid

A **setoid** is any type with a notion of **equivalence**.

Setoids must follow these laws:

- **Reflexivity**: `a.equals(a) === true`
- **Symmetry**/**Commutativity**: `a.equals(b) === b.equals(a)`
- **Transitivity**: If `a.equals(b)` and `b.equals(c)` then it must be `a.equals(c)`.

#### Introduced in this chapter

```
nub :: Setoid a => [a] -> [a]
```

`nub` returns a copy of the given array with the duplicates removed.

## 04 - Ord

**Definition**

> `Ord` types are types with **total** ordering.
> Every `Ord` is a `Setoid`.

That means that, given **any** two values of a given `Ord` type, you can determine whether one be greater than the other.

** `<=` / Less-Than-Or-Equal / lte**

```
lte :: Ord a => a ~> a -> Boolean
```

### Laws for `Ord`

> `Ord` defines less than or equal

- **Totality**: `a.lte(b) || b.lte(a) === true`
- **Antisymmetry**: `a.lte(b) && b.lte(a) === a.equals(b)`
- **Transitivity**: `a.lte(b) && b.lte(c) === a.lte(c)s`

## 05 - Semigroup

> A `Semigroup` type is one that has some concept of _combining_ values (via `concat`).

> With a Semigroup type, you can combine one or more values to make another.

> A valid `Semigroup` must have a concat method with the following signature:

```
concat :: Semigroup a => a ~> a -> a
```

A `Semigroup`'s `concat` method must take anothe rvalue of the same type, and return a third value of the same type.

The only law we have to worry about here is **_associativity_**:

```
a.concat(b).concat(c) === a.concat(b.concat(c))
```

## 06 - Monoid

A `Monoid` typs is any `Semigroup` type that happens to hae a special value - the **identity** value - stored on the type as a functional called `empty`.

> With a Monoid type, you can combine one or more values to make **zero or more** types.

> As a surprisingly good intuition, monoids encapsulate the logic of `Array.reduce`.

Signature

```
    empty :: Monoid m => () -> m
```

#### Identity laws

Laws for how `empty` must act for a type to be a valid `Monoid`:

> Whichever side of `concat` we put our `empty`, it _must_ make **no difference** to the value.

```
// Right identity
MyType(x).concat(MyType.empty()) === MyType(x)

// Left identity
MyType.empty().concat(MyType(x)) === MytType(x)
```

### Type Representations

> The fiddly part about monoids in JavaScript is that we have to pass in _type representations_ (what we called `M`).

```
// How do we know which `empty` we want? In
// Haskell, the correct `empty` would be used
// because the type would be checked to find the
// right monoid instance in the context.
const concatAll = xs => xs.reduce(concat, empty)

// In JS, the TypeRep avoids this issue.
const concatAll_ = M => xs =>
  xs.reduce(concat, M.empty())
```
